﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6.SecondPhase
{
    class Counter
    {
        public DateTime datetime;
        public int count;
        public List<String> addresses = new List<string>();

        public Counter(DateTime datetime)
        {
            this.datetime = new DateTime(datetime.Year, datetime.Month, datetime.Day, datetime.Hour, 0, 0);
            this.count = 0;
        }

        public void increase(String address)
        {
            if (!addresses.Contains(address))
            {
                addresses.Add(address);
                count = count + 1;
            }
        }

        public Boolean isTheHour(DateTime time)
        {
            return (datetime == new DateTime(time.Year, time.Month, time.Day, time.Hour, 0, 0));
        }
    }
}
