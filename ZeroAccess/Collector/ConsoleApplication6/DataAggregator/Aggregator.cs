﻿using ConsoleApplication6;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AggregatorFinal.DataAggregator
{
    class Aggregator
    {
        public static String targetFolder = "aggregatedData";
        public static String resultFolder = "collectedData";
        public static String reportFolder = "reports";
        Thread thread = new Thread(threadFunction);
        public Aggregator()
        {
            thread.Start();
            
        }

        static void threadFunction()
        {
            FirstPhase.handel(resultFolder, targetFolder);
        }
    }
}
