﻿using AggregatorFinal.DataAggregator;
using AgrigateData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class FirstPhase
    {
        public static void handel(String input, String Output)
        {


            String[] dirs = Directory.GetDirectories(input);

                DirectoryInfo d = new DirectoryInfo(dirs[0]);//Assuming Test is your Folder
                FileInfo[] files = d.GetFiles(); //Getting Text files

                foreach (FileInfo file in files)
                {
                    bool ok = !File.Exists(Aggregator.targetFolder + "/" + file.Name);

                    if (ok)
                    {
                        foreach (String dir in dirs)
                        {
                            if (!File.Exists(dir + "/" + file.Name))
                            {
                                ok = false;
                                break;
                            }
                        }
                    }

                    if (ok)
                    {
                        if (file.Extension.Contains("gz") || file.Extension.Contains("rar"))
                        {
                            FileItem.init();

                            foreach (String dir in dirs)
                            {
                                FileInfo tempFileInfo = new FileInfo(dir + "/" + file.Name);
                                Zipper.Decompress(tempFileInfo);
                                FileItem.addFileItem(dir + "/" + file.Name.Replace(".gz", ""));
                                FileInfo fileInfo = new FileInfo(dir + "/" + file.Name.Replace(".gz", ""));
                                fileInfo.Delete();
                            }


                            String target = Output + "/" + file.Name.Replace(".gz", "");
                            FileItem.writeFile(target);

                            Console.WriteLine("Aggregated file: " + file.Name );

                            FileInfo fileInfo1 = new FileInfo(target);
                            Zipper.Compress(fileInfo1);
                            fileInfo1.Delete();

                        }
                    }
                }
            

        }

        private static List<String> DirSearch(string sDir)
        {
            List<String> files = new List<String>();
            try
            {
                foreach (string f in Directory.GetFiles(sDir))
                {
                    files.Add(f);
                }
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    files.AddRange(DirSearch(d));
                }
            }
            catch (System.Exception excpt)
            {

            }

            return files;
        }
    }

}
