﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace AggregatorFinal
{
    class UDP_Receiver
    {
        public UdpClient udpClient;
        string receiverPort;
        string receiverIP;


        public UDP_Receiver(string recPort)
        {
            udpClient = new UdpClient(Convert.ToInt32(recPort));
            receiverPort = ((IPEndPoint)udpClient.Client.LocalEndPoint).Port.ToString();
            receiverIP = ((IPEndPoint)udpClient.Client.LocalEndPoint).Address.ToString();
        }

        public string receiveMessage()
        {

            var remoteEP = new IPEndPoint(IPAddress.Any, 0);
            byte[] data = udpClient.Receive(ref remoteEP);
            string message = Encoding.ASCII.GetString(data, 0, data.Length);
            return message;
        }


        public string getPort()
        {
            return receiverPort;
        }

    }
}
