﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgrigateData
{
    class Item
    {
        public String sourceIP;
        public String sourcePort;
        public String desIP;
        public String desPort;
        public DateTime datetime;

        public static int secondsToAdd = 0;
        public static String fileName;
        public static int addSeconds = 0;

        public Item(String sourceIP, String sourcePort, String desIP, String desPort)
        {
            this.sourceIP = sourceIP;
            this.sourcePort = sourcePort;
            this.desIP = desIP;
            this.desPort = desPort;
        }

        public Item(String line)
        {
            String[] firstSplitter = { "///" };
            String[] secondSplitter = { "#" };
            String[] firstParts = line.Split(firstSplitter, StringSplitOptions.None);
            String[] sourceParts = firstParts[0].Split(secondSplitter, StringSplitOptions.None);
            String[] desParts = firstParts[1].Split(secondSplitter, StringSplitOptions.None);

            sourceIP = sourceParts[0];
            sourcePort = sourceParts[1];
            desIP = desParts[0];
            desPort = desParts[1];
            datetime = DateTime.Parse(firstParts[3]).AddSeconds(-secondsToAdd);
        }
    }
}
