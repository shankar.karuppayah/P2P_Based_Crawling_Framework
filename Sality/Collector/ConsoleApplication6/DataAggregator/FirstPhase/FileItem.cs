﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgrigateData
{
    class FileItem
    {
        public static List<Item> list;

        public static void init()
        {
            list = new List<Item>();
        }

        public static void addFileItem(String path)
        {

            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            String[] splitters = { "##" };
            String[] parts;
            TimeSpan timespan;
            while ((line = file.ReadLine()) != null)
            {
                if (line.Contains("##"))
                {
                    
                    parts = line.Split(splitters, StringSplitOptions.None);
                    timespan = DateTime.Parse(parts[0]).Subtract(DateTime.Parse(parts[1]));
                    Item.secondsToAdd = (int)timespan.TotalSeconds;
                    continue;
                }
                if (line.Contains("///"))
                {
                    Item item = new Item(line);
                    list.Add(item);
                }
            }

            file.Close();
        }


        public static void writeFile(String path)
        {
            list = SortAscending(list);
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {

                    foreach (Item item in list)
                    {
                        String s = "";
                        if (item.sourceIP.Length <= 13)
                        { s = "\t \t: "; }
                        else
                        { s = " \t: "; }
                        if (item.sourceIP.Length <= 10)
                        { s = "\t \t\t: "; }
                        String s1 = "";
                        if (item.desIP.Length <= 13)
                        { s1 = "\t \t: "; }
                        else
                        { s1 = " \t: "; }
                        if (item.desIP.Length <= 10)
                        { s1 = "\t \t\t: "; }
                        sw.WriteLine(item.datetime.ToString("MM/dd/yyyy hh:mm:ss.fff tt")
                            + " # " + item.sourceIP
                            + s + item.sourcePort
                            + "\t# " + item.desIP
                            + s1 + item.desPort);
                    }

                }
            }
        }

        static List<Item> SortAscending(List<Item> list)
        {
            list.Sort((a, b) => a.datetime.CompareTo(b.datetime));
            return list;
        }
    }
}
