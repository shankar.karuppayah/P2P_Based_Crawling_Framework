﻿using AggregatorFinal.DataAggregator;
using AggregatorFinal.Rec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AggregatorFinal
{
    class RecThread
    {
        Thread _thread;
        String filename;
        int port;

        public RecThread(String filename, int port)
        {
            this.filename = filename;
            this.port = port;
            _thread = new Thread(threadFunction);
            _thread.Start();
        }

        private void threadFunction()
        {
            Server.startLis(port, filename);
        }
    }
}
