﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace AggregatorFinal.Rec
{
    class Server
    {
        static int BufferSize = 500;

        public static void startLis(int port, String filename)
        {
            TcpListener Listener = null;
            try
            {
                Listener = new TcpListener(IPAddress.Any, port);
                Listener.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            byte[] RecData = new byte[BufferSize];
            int RecBytes;

            TcpClient client = null;
            NetworkStream netstream = null;
            try
            {
                client = Listener.AcceptTcpClient();
                netstream = client.GetStream();

                int totalrecbytes = 0;
                FileStream Fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write);
                while ((RecBytes = netstream.Read(RecData, 0, RecData.Length)) > 0)
                {
                    Fs.Write(RecData, 0, RecBytes);
                    totalrecbytes += RecBytes;
                }
                Fs.Close();

                netstream.Close();
                client.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
