﻿using AggregatorFinal.DataAggregator;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AggregatorFinal.Rec
{
    class MainThread
    {
        Thread _thread;
        String ip;
        string port;

        public MainThread(String ip, string port)
        {
            this.port = port;
            this.ip = ip;
            _thread = new Thread(threadFunction);
            _thread.Start();
        }

        private void threadFunction()
        {
            try
            {
                char[] splitter = { '#' };
                UDP_Receiver _udp_Receiver = new UDP_Receiver(port);
                while (true)
                {

                    string text = _udp_Receiver.receiveMessage();
                    String[] parts2 = text.Split(splitter, StringSplitOptions.None);

                    String senderIp = parts2[0];
                    String senderPort = parts2[1];
                    String fileName = parts2[2];

                    DirectoryInfo di = Directory.CreateDirectory(Aggregator.resultFolder + "/" + senderIp);


                    int freePort = FreeTcpPort();


                    new RecThread(di.FullName + "/" + fileName, freePort);

                    Console.WriteLine("Rec file: " + fileName + ", from: " + senderIp);

                    UDP_Sender.forwardMessage(senderIp, senderPort, "RequestFile" + "#" + fileName + ":" + ip + ":" + freePort);

                }

            }
            catch (Exception es)
            {
                Console.WriteLine(es.Message);
            }
        }

        static int FreeTcpPort()
        {
            TcpListener l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            return port;

        }
    }
}
